require 'test_helper'

class TeamTest < ActiveSupport::TestCase
  test "contains 3 monsters" do
    assert teams(:one).monsters.count == 3
  end

  test "is invalid with less than 3 monsters" do
    one = teams(:one)
    one.monsters.last.destroy
    assert_not one.valid?
  end

  test "is invalid with more than 3 monsters" do
    one = teams(:one)
    one.monsters << Monster.new
    assert_not one.valid?
  end
end
