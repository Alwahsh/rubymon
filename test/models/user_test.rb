require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test "can have a maximum of 3 teams" do
    user = users(:one)
    3.times do
      assert user.valid?, user.errors.full_messages
      user.teams << teams(:one)
    end
    assert_not user.valid?, "having #{user.teams.count} teams but still valid"
  end

  test "can have a maximum of 20 monsters" do
    user = users(:two)
    20.times do
      assert user.valid?, user.errors.full_messages
      user.monsters << monsters(:one)
    end
    assert_not user.valid?, "having #{user.monsters.count} monsters but still valid"
  end
end
