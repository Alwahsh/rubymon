require 'test_helper'

class MonsterTest < ActiveSupport::TestCase
  test "has a type" do
    assert monsters(:one).mtype = "fire"
  end

  test "gets strength of a type" do
    assert monsters(:one).weakness == "water", "expected water got " + monsters(:one).weakness
  end

  test "gets weakness of a type" do
    assert monsters(:one).strength == "wind", "expected wind got " + monsters(:one).strength
  end
end
