module ApplicationHelper

  def get_user
    return User.find_by(id: session[:user_id]) if session[:user_id]
    return User.find_by(token: params[:token]) if params[:token]
    return nil
  end

  def cur_user
    @cur_user ||= get_user
  end

  def enforce_auth
    unless cur_user
      if request.format == 'json'
        render json: "Must be logged in"
      else
        redirect_to "/auth/facebook" unless cur_user
      end
    end
  end
end
