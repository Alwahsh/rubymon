module SessionsHelper
  def web_login_notice(user)
    if user.plain_password
      return t(:successful_register) + "id: #{user.id} password: #{user.plain_password}"
    else
      return t(:successful_login)
    end
  end
end
