json.array!(@monsters) do |monster|
  json.extract! monster, :id, :name, :power, :mtype
  json.url monster_url(monster, format: :json)
end
