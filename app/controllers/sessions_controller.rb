class SessionsController < ApplicationController
  include SessionsHelper

  def create
    user = User.omniauth(env['omniauth.auth'])
    session[:user_id] = user.id
    redirect_to monsters_url, notice: web_login_notice(user)
  end

  # For android authentication
  def auth
    user = User.auth(params[:id], params[:password])
    if user
      render json: user.to_json(only: :token)
    else
      render json: "Incorrect data"
    end
  end
end
