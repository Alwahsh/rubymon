class User < ActiveRecord::Base
  cattr_accessor :plain_password
  has_many :teams
  has_many :monsters

  validates_length_of :teams, maximum: 3
  validates_length_of :monsters, maximum: 20

  class << self
    def encrypt(str)
      Digest::SHA2.hexdigest(str)
    end

    def omniauth(auth)
      user = User.find_or_initialize_by(uid: auth.uid)
      return user if user.id
      user.name = auth.info.name
      user.fb_token = auth.credentials.token
      user.salt = SecureRandom.base64(8)
      user.plain_password = SecureRandom.base64(8)
      user.password = encrypt(user.salt + user.plain_password)
      user.save!
      return user
    end

    def auth(id, password)
      user = User.find_by(id: id)
      user.token = SecureRandom.urlsafe_base64
      user.save
      return user if user && encrypt(user.salt + password) == user.password
      nil
    end
  end
end
