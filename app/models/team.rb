class Team < ActiveRecord::Base
  belongs_to :user
  has_many :monsters
  validates_length_of :monsters, is: 3
  accepts_nested_attributes_for :monsters
end
