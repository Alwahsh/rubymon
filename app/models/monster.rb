class Monster < ActiveRecord::Base
  enum mtype: [:fire, :water, :earth, :electric, :wind]

  belongs_to :user
  belongs_to :team

  validates_presence_of :user

  # Get the strength of the monster.
  def strength
    Monster.mtypes.key(
      (Monster.mtypes[mtype] - 1) % Monster.mtypes.count
    )
  end

  # Get the weakness of the monster.
  def weakness
    Monster.mtypes.key(
      (Monster.mtypes[mtype] + 1) % Monster.mtypes.count
    )
  end

end
