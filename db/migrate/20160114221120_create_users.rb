class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :uid
      t.string :name
      t.string :salt
      t.string :password
      t.string :fb_token
      t.string :token

      t.timestamps null: false
    end
  end
end
