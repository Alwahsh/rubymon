class CreateMonsters < ActiveRecord::Migration
  def change
    create_table :monsters do |t|
      t.string :name
      t.float :power
      t.integer :mtype

      t.timestamps null: false
    end
  end
end
