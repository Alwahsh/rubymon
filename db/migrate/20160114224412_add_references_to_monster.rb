class AddReferencesToMonster < ActiveRecord::Migration
  def change
    add_reference :monsters, :user, index: true, foreign_key: true
    add_reference :monsters, :team, index: true, foreign_key: true
  end
end
